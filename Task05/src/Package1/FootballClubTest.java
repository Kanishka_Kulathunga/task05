package Package1;

public class FootballClub implements Comparable<FootballClub>{
	private int position;
	private String club;
	private int match;
	private int win;
	private int lost;
	private int pointDif;
	private int points;
	
	private FootballClub(int position, String club,int match,int win,int lost,int pointsDif,int points) {
		this.position=position;
		this.club=club;
		this.match=match;
		this.win=win;
		this.lost=lost;
		this.pointDif=pointsDif;
		this.points=points;
	}
	
	public String toString() {
		return String.format("%-3d%-20s%-10d%-10d%-10d", position,club,points,win,lost);
	}
	
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position=position;
	}
	
	public String getClub() {
		return club;
	}
	public void setClub(String club) {
		this.club=club;
	}
	
	public int getMatch() {
		return match;
	}
	public void setMatch(int match) {
		this.match=match;
	}
	
	public int getWin() {
		return win;
	}
	public void setWin(int win) {
		this.win=win;
	}
	public int getLost() {
		return lost;
	}
	public void setLost(int lost) {
		this.lost=lost;
	}
	public int getPointDif() {
		return pointDif;
	}
	public void setPointDif(int pointDif) {
		this.pointDif=pointDif;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points=points;
	}
	
	public int compareTo(FootballClub 0) {
		//TODO Auto-generated method stub
		return 0;
	}
	
}
